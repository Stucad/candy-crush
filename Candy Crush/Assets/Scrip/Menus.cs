using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menus : MonoBehaviour
{
  public void Juego1()
  {
    SceneManager.LoadScene("Level 1");
  }//LLama scene del nivel 1    
  public void Juego2()
  {
      SceneManager.LoadScene("Level 2");
    }// LLama scene del nivel 2
  public void Juego3()
  {
      SceneManager.LoadScene("Level 3");
    }// LLama scene del nivel 3
  public void Juego4()
  {
      SceneManager.LoadScene("Level 4");
    }// LLama scene del nivel 4
  public void Juego5()
  {
      SceneManager.LoadScene("Level 5");
    }// LLama scene del nivel 5
  public void Reglas()
  {
      SceneManager.LoadScene("Reglas");
    }// LLama scene del las Reglas
  public void Menu()
  {
    SceneManager.LoadScene("Menu");
    } // LLama scene del Menu
  public void Levels()
  {
     SceneManager.LoadScene("Niveles");
    }// LLama scene para seleccionar los niveles
  public void Exit()
  {
    Application.Quit();
    }// Hace cerrar el juego en el ejecutable 
}
