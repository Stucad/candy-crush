using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    //Cordenadas
    public int xIndex;
    public int yIndex;
    //Audio 
    AudioSource souce;
    public AudioClip audioFx;
    Board m_board;
    public void Init(int cambioX, int cambioY, Board board)
    {
        xIndex = cambioX;
        yIndex = cambioY;

        m_board = board;
    }// Conrdenadas del tile

    public void OnMouseDown()
    {
        m_board.ClickedTile(this);
    }// Funcion para que detecte el mouse ensima del tile

    public void OnMouseEnter()
    {
        m_board.DragToTile(this);
    }//Funcion para que detecte cuando arrastes el tile

    public void OnMouseUp()
    {
        m_board.ReleaseTile();
        AudioSource.PlayClipAtPoint(audioFx, gameObject.transform.position);
    }//Funcion para soltar el tile 

}
