using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Puntaje : MonoBehaviour
{
    // Multiplicador de los puntos 
    private int puntos;
    // Variable publica para modificar los puntos necesarios para ganar
    public int puntajeNecesario;
    // Texto para ver los puntos en Juego 
    private TextMeshProUGUI TextMesh;

    private void Start()
    {
        TextMesh = GetComponent<TextMeshProUGUI>();
    }//Iniciar Texto
    private void Update()
    {
        TextMesh.text = puntos.ToString("0");
    }//Modifica texto
    public void SumatoriaPuntos(int puntosEntrada)
    {
        puntos += puntosEntrada;
       if(puntos >= puntajeNecesario)
        {
            SceneManager.LoadScene("Win");
        }
    }//Sumar puntos 

 
}
