using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Contador : MonoBehaviour
{
    // Entero de los minutos y seg
    public int min, seg;
    // Texto para ver el tiempo en Unity
    public TMP_Text tiempo;
    //Multiplicador de los minutos
    public float restante;
    // bool para saber cuando esta corriendo el juego 
    public bool enMarcha;

    private void Awake()
    {
        restante = (min * 60) + seg;
    }// Multiplica los minutos

    // Update is called once per frame
    void Update()
    {
        if(enMarcha)
        {
            restante -= Time.deltaTime;
            if(restante < 1)
            {
              SceneManager.LoadScene("GameOver");
            }
            int tempMin = Mathf.FloorToInt(restante / 60);
            int tempSeg = Mathf.FloorToInt(restante % 60);

            tiempo.text = string.Format("{00:00} : {01:00}", tempMin, tempSeg);
        }
    }// Hace que se mueva el contador con los minutos 
}
